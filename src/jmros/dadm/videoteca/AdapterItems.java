package jmros.dadm.videoteca;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class AdapterItems extends BaseAdapter {

	protected Activity activity;
	protected ArrayList<Item> items;

	public AdapterItems(Activity activity, ArrayList<Item> items) {
		this.activity = activity;
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int arg0) {
		return items.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// Generamos una convertView por motivos de eficiencia
		View v = convertView;

		// Asociamos el layout de la lista que hemos creado
		if (convertView == null) {
			LayoutInflater inf = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inf.inflate(R.layout.item_lista, null);
		}

		// Creamos un objeto directivo
		Item dir = items.get(position);
		// Rellenamos el nombre

		ImageView idiomaImg = (ImageView) v.findViewById(R.id.fila_idioma);
		String value = dir.getIdioma();
		if ("Español".equals(value)) {
			idiomaImg.setImageResource(R.drawable.peliesp);
		} else if ("Inglés".equals(value)) {
			idiomaImg.setImageResource(R.drawable.pelieng);
		} else if ("Francés".equals(value)) {
			idiomaImg.setImageResource(R.drawable.pelifra);
		} else if ("Vasco".equals(value)) {
			idiomaImg.setImageResource(R.drawable.pelivas);
		} else if ("Gallego".equals(value)) {
			idiomaImg.setImageResource(R.drawable.peligal);
		} else if ("Catalán".equals(value)) {
			idiomaImg.setImageResource(R.drawable.librocat);
		} else if ("Aleman".equals(value)) {
			idiomaImg.setImageResource(R.drawable.peliale);
		} else if ("Italiano".equals(value)) {
			idiomaImg.setImageResource(R.drawable.peliita);
		} else if ("Chino".equals(value)) {
			idiomaImg.setImageResource(R.drawable.pelichi);
		}

		ImageView formatoImg = (ImageView) v.findViewById(R.id.fila_imagen_formato);
		String valueFormato = dir.getFormato();
		if ("dvd".equals(valueFormato)) {
			formatoImg.setImageResource(R.drawable.dvd);
		} else if ("vhs".equals(valueFormato)) {
			formatoImg.setImageResource(R.drawable.vhs);
		} else if ("blueray".equals(valueFormato)) {
			formatoImg.setImageResource(R.drawable.bluray);
		}

		TextView titulo = (TextView) v.findViewById(R.id.fila_titulo);
		titulo.setText(dir.getTitulo());
		TextView autor = (TextView) v.findViewById(R.id.fila_autor);
		autor.setText(dir.getDirector());

		Float valoracionFloat = 0f;
		if (null != dir.getValoracion() && !dir.getValoracion().equals(0)) {
			valoracionFloat = dir.getValoracion();
		}
		RatingBar valoracion = (RatingBar) v.findViewById(R.id.fila_valoracion);
		valoracion.setRating(valoracionFloat);
		// id_pelicula = (Long.parseLong(extras.getString("id_pelicula")));
		String strLong = "no definido";
		if (null != dir.getFecha_fin_prestamo() && dir.getFecha_fin_prestamo() != 0) {
			strLong = new SimpleDateFormat("dd/MM/yyyy").format((Long) dir
					.getFecha_fin_prestamo());
		}
		TextView fecha = (TextView) v.findViewById(R.id.fila_fecha);
		fecha.setText(strLong);

		ImageView notasImg = (ImageView) v.findViewById(R.id.fila_notas_imagen);
		String str = dir.getNotas();
		if (str != null && !("").equals(str)) {
			notasImg.setImageResource(R.drawable.checked);
		} else {
			notasImg.setImageResource(R.drawable.unchecked);
		}
		ImageView finalizadoImg = (ImageView) v
				.findViewById(R.id.fila_leido_imagen);
		if (0 == dir.getFecha_fin_prestamo()) {
			finalizadoImg.setImageResource(R.drawable.unchecked);
		} else {
			finalizadoImg.setImageResource(R.drawable.checked);
		}

		ImageView prestadoImg = (ImageView) v
				.findViewById(R.id.fila_prestado_imagen);
		if (0 == dir.getFecha_ini_prestamo()) {
			prestadoImg.setImageResource(R.drawable.unchecked);
		} else {
			prestadoImg.setImageResource(R.drawable.checked);
		}

		// Rellenamos el cargo
		/*
		 * TextView idioma = (TextView) v.findViewById(R.id.idioma);
		 * idioma.setText(dir.getIdioma());
		 */
		// Retornamos la vista
		return v;
	}
}
