package jmros.dadm.videoteca;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private SQLiteDatabase db;
	ArrayList<Item> registros;

	Button buttonFirstNewFilm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_contextual_item, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_newFilm) {
			Intent SingleVideoActivity = new Intent(getBaseContext(),
					SingleVideoActivity.class);
			startActivity(SingleVideoActivity);
			return true;
		}
		if (id == R.id.action_About) {
			Intent AboutUsActivity = new Intent(getBaseContext(),
					AboutUsActivity.class);
			startActivity(AboutUsActivity);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		super.onResume();
		videotecaSQLiteHelper usdbh = new videotecaSQLiteHelper(this,
				"dbpeliculas.db", null, 1);
		db = usdbh.getReadableDatabase();
		registros = new ArrayList<Item>();

		ListView listaDatos = (ListView) findViewById(R.id.listaPeliculas);

		listaDatos.setAdapter(new ArrayAdapter<Item>(this,
				android.R.layout.activity_list_item, registros));

		String sqlQuery = "SELECT * FROM peliculas";
		Cursor cursor = db.rawQuery(sqlQuery, null);

		registros.clear();

		if (cursor.moveToFirst()) {
			Toast.makeText(this, "Número de Registros: " + cursor.getCount(),
					Toast.LENGTH_SHORT).show();
			do {
				// registros.add(cursor.getString(1))
				Long id_pelicula = cursor.getLong(0);
				String genero = cursor.getString(1);
				String titulo = cursor.getString(2);
				String director = cursor.getString(3);
				String idioma = cursor.getString(4);
				Long fechaIni = cursor.getLong(5);
				Long fechaFin = cursor.getLong(6);
				String prestadoA = cursor.getString(7);
				Float valoracion = cursor.getFloat(8);
				String formato = cursor.getString(9);
				String nota = cursor.getString(10);

				Item pelicula = new Item(id_pelicula, genero, titulo, director, idioma,
						fechaIni, fechaFin, prestadoA, valoracion, formato, nota);
				registros.add(pelicula);

			} while (cursor.moveToNext());
		}

		AdapterItems adapter = new AdapterItems(this, registros);
		listaDatos.setAdapter(adapter);
		// listaDatos.setAdapter(new
		// ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
		// registros));

		// indicamos al listview que tiene un menu contextual
		registerForContextMenu(listaDatos);

		listaDatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {

				Intent i = new Intent(getApplicationContext(),
						SingleVideoActivity.class);
				Item pelicula = registros.get(position);
				i.putExtra("id_pelicula", String.valueOf(pelicula.getId()));
				startActivity(i);

			}

		});

		db.close();

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		// info.position devuelve la posicion del item en el menu
		switch (item.getItemId()) {
		case R.id.menu_contextual_borrar:
			borarItemDialog(info.position);

			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	private void borarItemDialog(Integer posicion) {
		final Item pelicula = registros.get(posicion);
		new AlertDialog.Builder(this)
				.setTitle("Borrar pelicula")
				.setMessage("¿Estás seguro que deseas borrar la película?")
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								borrarItem(pelicula.getId());
							}
						})
				.setNegativeButton(android.R.string.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// do nothing
							}
						}).setIcon(android.R.drawable.ic_dialog_alert).show();

	}

	private void borrarItem(Long idItem) {
		videotecaSQLiteHelper usdbh = new videotecaSQLiteHelper(this,
				"dbpeliculas.db", null, 1);
		db = usdbh.getWritableDatabase();
		db.execSQL("DELETE FROM peliculas WHERE _id = '" + String.valueOf(idItem)
				+ "'");
		db.close();
		Toast.makeText(this, "Película borrada", Toast.LENGTH_SHORT).show();
		onResume();
	}
}
