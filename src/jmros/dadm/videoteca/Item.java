package jmros.dadm.videoteca;

//import android.graphics.Drawable;

public class Item {
	protected long id;
	protected String genero;
	protected String titulo;
	protected String director;
	protected String idioma;
	protected Long fecha_ini_prestamo;
	protected Long fecha_fin_prestamo;
	protected String prestado_a;
	protected Float valoracion;
	protected String formato;
	protected String notas;

	public Item(String genero, String titulo, String director, String idioma,
			Long fecha_ini_prestamo, Long fecha_fin_prestamo, String prestado_a,
			Float valoracion, String formato, String notas) {
		super();
		this.genero = genero;
		this.titulo = titulo;
		this.director = director;
		this.idioma = idioma;
		this.fecha_ini_prestamo = fecha_ini_prestamo;
		this.fecha_fin_prestamo = fecha_fin_prestamo;
		this.prestado_a = prestado_a;
		this.valoracion = valoracion;
		this.formato = formato;
		this.notas = notas;
	}

	public Item(long id, String genero, String titulo, String director,
			String idioma, Long fecha_ini_prestamo, Long fecha_fin_prestamo,
			String prestado_a, Float valoracion, String formato, String notas) {
		super();
		this.id = id;
		this.genero = genero;
		this.titulo = titulo;
		this.director = director;
		this.idioma = idioma;
		this.fecha_ini_prestamo = fecha_ini_prestamo;
		this.fecha_fin_prestamo = fecha_fin_prestamo;
		this.prestado_a = prestado_a;
		this.valoracion = valoracion;
		this.formato = formato;
		this.notas = notas;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public Long getFecha_ini_prestamo() {
		return fecha_ini_prestamo;
	}

	public void setFecha_ini_prestamo(Long fecha_ini_prestamo) {
		this.fecha_ini_prestamo = fecha_ini_prestamo;
	}

	public Long getFecha_fin_prestamo() {
		return fecha_fin_prestamo;
	}

	public void setFecha_fin_prestamo(Long fecha_fin_prestamo) {
		this.fecha_fin_prestamo = fecha_fin_prestamo;
	}

	public String getPrestado_a() {
		return prestado_a;
	}

	public void setPrestado_a(String prestado_a) {
		this.prestado_a = prestado_a;
	}

	public Float getValoracion() {
		return valoracion;
	}

	public void setValoracion(Float valoracion) {
		this.valoracion = valoracion;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

}
