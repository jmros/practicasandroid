package jmros.dadm.videoteca;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class videotecaSQLiteHelper extends SQLiteOpenHelper {

	// Sentencia SQL para crear la tabla de peliculas

	String sqlCreate = "CREATE TABLE peliculas (" + "_id integer primary key, "
			+ "genero text not null, " + "titulo text not null, "
			+ "director text not null, " + "idioma text, "
			+ "fecha_ini_prestamo long, " + "fecha_fin_prestamo long, "
			+ "pestado_a text, " + "valoracion  float, " + "formato text, "
			+ "notas text" + ")";

	public videotecaSQLiteHelper(Context contexto, String nombre,
			CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci?n de la tabla
		db.execSQL(sqlCreate);
		db.execSQL("INSERT INTO peliculas (genero, titulo,director,idioma,fecha_ini_prestamo, fecha_fin_prestamo, pestado_a,valoracion,formato,notas)"
				+ " VALUES ('Suspense','pelicula 1','director 1','español',1401854400000,1401854400000,'pestado 1', 4,'dvd','notas 1')");
		db.execSQL("INSERT INTO peliculas (genero, titulo,director,idioma,fecha_ini_prestamo, fecha_fin_prestamo, pestado_a,valoracion,formato,notas)"
				+ " VALUES ('Suspense','pelicula 2','director 2','Catalán',1401854400000,null,'pestado 2', 4,'blueray','notas 2')");
		db.execSQL("INSERT INTO peliculas (genero, titulo,director,idioma,fecha_ini_prestamo, fecha_fin_prestamo, pestado_a,valoracion,formato,notas)"
				+ " VALUES ('Acción','pelicula 3','director 3','español',null,1401854400000,'pestado 3', 4,'vhs','notas 3')");
		db.execSQL("INSERT INTO peliculas (genero, titulo,director,idioma,fecha_ini_prestamo, fecha_fin_prestamo, pestado_a,valoracion,formato,notas)"
				+ " VALUES ('Suspense','pelicula 4','director 4','español',null,null,'pestado 4', 4,'dvd','notas 4')");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
		// NOTA: Por simplicidad del ejemplo aqu? utilizamos directamente
		// la opci?n de eliminar la tabla anterior y crearla de nuevo
		// vac?a con el nuevo formato.
		// Sin embargo lo normal ser? que haya que migrar datos de la
		// tabla antigua a la nueva, por lo que este m?todo deber?a
		// ser m?s elaborado.

		// Se elimina la versi?n anterior de la tabla
		db.execSQL("DROP TABLE IF EXISTS peliculas");

		// Se crea la nueva versi?n de la tabla
		db.execSQL(sqlCreate);
	}

}
