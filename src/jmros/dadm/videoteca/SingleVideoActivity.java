package jmros.dadm.videoteca;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class SingleVideoActivity extends ActionBarActivity {

	private SQLiteDatabase db;

	Long id_pelicula;
	Spinner genero;
	TextView titulo;
	TextView director;
	Spinner idioma;
	TextView fechaInicio;
	TextView fechaFin;
	TextView prestadoA;
	RatingBar valoracion;
	Spinner formato;
	TextView notas;
	Context context;

	/*
	 * protected String genero; protected String titulo; protected String
	 * director; protected String idioma; protected Long fecha_ini_prestamo;
	 * protected Long fecha_fin_prestamo; protected String prestado_a; protected
	 * Float valoracion; protected String formato; protected String notas;
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_single_video);

		// definición del formulario
		genero = (Spinner) findViewById(R.id.single_videoSpinnerTipo);
		titulo = (TextView) findViewById(R.id.single_videoTextTitulo);
		director = (TextView) findViewById(R.id.single_videoTextDirector);
		idioma = (Spinner) findViewById(R.id.single_videoSpinnerIdioma);
		fechaInicio = (TextView) findViewById(R.id.single_videoFechaInicioValue);
		fechaFin = (TextView) findViewById(R.id.single_videoFechaFinValue);
		valoracion = (RatingBar) findViewById(R.id.single_videoRatingValoracion);
		prestadoA = (TextView) findViewById(R.id.single_videoTextPrestadoA);
		formato = (Spinner) findViewById(R.id.single_videoSpinnerFormato);
		notas = (TextView) findViewById(R.id.single_videoTextNotas);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			videotecaSQLiteHelper usdbh = new videotecaSQLiteHelper(this,
					"dbpeliculas.db", null, 1);
			db = usdbh.getReadableDatabase();

			id_pelicula = (Long.parseLong(extras.getString("id_pelicula")));
			// Toast.makeText(this, "he recibido el item id: " +
			// id_pelicula.toString(),
			// Toast.LENGTH_SHORT).show();

			// hacemos la petición para obtener el registro
			String sqlQuery = "SELECT * FROM peliculas WHERE _id='"
					+ id_pelicula.toString() + "'";
			Cursor cursor = db.rawQuery(sqlQuery, null);

			if (cursor.moveToFirst()) {
				id_pelicula = cursor.getLong(0);

				/*
				 * String genero = cursor.getString(1); String titulo =
				 * cursor.getString(2); String director = cursor.getString(3); String
				 * idioma = cursor.getString(4); Long fechaIni = cursor.getLong(5); Long
				 * fechaFin = cursor.getLong(6); String prestadoA = cursor.getString(7);
				 * Float valoracion = cursor.getFloat(8); String formato =
				 * cursor.getString(9); String nota = cursor.getString(10);
				 */

				int spinnerPosition;
				@SuppressWarnings("unchecked")
				ArrayAdapter<String> myAdapGenero = (ArrayAdapter<String>) genero
						.getAdapter(); // cast to an ArrayAdapter
				spinnerPosition = myAdapGenero.getPosition(cursor.getString(1));
				// set the default according to value
				genero.setSelection(spinnerPosition);
				titulo.setText(cursor.getString(2));
				director.setText(cursor.getString(3));
				@SuppressWarnings("unchecked")
				ArrayAdapter<String> myAdapIdioma = (ArrayAdapter<String>) idioma
						.getAdapter(); // cast to an ArrayAdapter
				spinnerPosition = myAdapIdioma.getPosition(cursor.getString(4));
				// set the default according to value
				idioma.setSelection(spinnerPosition);
				if (cursor.getString(5) != null) {
					fechaInicio.setText(new SimpleDateFormat("dd/MM/yyyy")
							.format((Long) Long.parseLong(cursor.getString(5))));
				} else {
					fechaInicio.setText(null);
				}
				if (cursor.getString(6) != null) {
					fechaFin.setText(new SimpleDateFormat("dd/MM/yyyy")
							.format((Long) Long.parseLong(cursor.getString(6))));
				} else {
					fechaFin.setText(null);
				}
				prestadoA.setText(cursor.getString(7));
				valoracion.setRating(cursor.getFloat(8));
				@SuppressWarnings("unchecked")
				ArrayAdapter<String> myAdapFormato = (ArrayAdapter<String>) formato
						.getAdapter(); // cast to an ArrayAdapter
				spinnerPosition = myAdapFormato.getPosition(cursor.getString(9));
				formato.setSelection(spinnerPosition);
				notas.setText(cursor.getString(10));

			}

			db.close();

		}

		//
		// elementos del boton de fecha de inicio
		//
		final OnDateSetListener dateSetListenerFechaInicio = new OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				// getCalender();
				int mYear = year;
				int mMonth = monthOfYear;
				int mDay = dayOfMonth;
				TextView v = (TextView) findViewById(R.id.single_videoFechaInicioValue);

				v.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mDay).append("/").append(mMonth + 1).append("/")
						.append(mYear).append(""));

			}
		};

		final Button button = (Button) findViewById(R.id.single_videoButtonFechaInicioLectura);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Calendar calendarioInicio = Calendar.getInstance();

				TextView textVidewFechaInicioLectura = (TextView) findViewById(R.id.single_videoFechaInicioValue);
				SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
				try {
					calendarioInicio.setTime(formatDate
							.parse((String) textVidewFechaInicioLectura.getText()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int mYear = calendarioInicio.get(Calendar.YEAR);
				int mMonth = calendarioInicio.get(Calendar.MONTH);
				int mDay = calendarioInicio.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog dialog = new DatePickerDialog(context,
						dateSetListenerFechaInicio, mYear, mMonth, mDay);
				dialog.show();

			}
		});

		//
		// elementos del boton de fecha de fin
		//
		final OnDateSetListener dateSetListenerFechaFin = new OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				// getCalender();
				int mYear = year;
				int mMonth = monthOfYear;
				int mDay = dayOfMonth;
				TextView textViewFechaFinLectura = (TextView) findViewById(R.id.single_videoFechaFinValue);

				textViewFechaFinLectura.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mDay).append("/").append(mMonth + 1).append("/")
						.append(mYear).append(""));

			}
		};

		final Button buttonFin = (Button) findViewById(R.id.single_videoButtonFechaFinLectura);
		buttonFin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Calendar calendarioFin = Calendar.getInstance();

				TextView v1 = (TextView) findViewById(R.id.single_videoFechaFinValue);
				SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

				try {
					calendarioFin.setTime(formatDate.parse((String) v1.getText()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int mYear = calendarioFin.get(Calendar.YEAR);
				int mMonth = calendarioFin.get(Calendar.MONTH);
				int mDay = calendarioFin.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog dialog = new DatePickerDialog(context,
						dateSetListenerFechaFin, mYear, mMonth, mDay);
				dialog.show();

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.single_video, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_save:
			guardarDatos();
			// return true;
		case R.id.menu_cancel:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void guardarDatos() {

		videotecaSQLiteHelper usdbh = new videotecaSQLiteHelper(this,
				"dbpeliculas.db", null, 1);
		db = usdbh.getWritableDatabase();

		Long fecha_inicio_milisegundos = null;
		Long fecha_fin_milisegundos = null;

		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
		try {
			fecha_inicio_milisegundos = formatDate.parse(
					(String) fechaInicio.getText()).getTime();
			fecha_fin_milisegundos = formatDate.parse((String) fechaFin.getText())
					.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (null == id_pelicula) {

			db.execSQL("INSERT INTO peliculas (genero, titulo,director,idioma,fecha_ini_prestamo, fecha_fin_prestamo, pestado_a,valoracion,formato,notas)"
					+ " VALUES ('"
					+ genero.getSelectedItem()
					+ "','"
					+ titulo.getText()
					+ "','"
					+ director.getText()
					+ "','"
					+ idioma.getSelectedItem()
					+ "','"
					+ fecha_inicio_milisegundos
					+ "','"
					+ fecha_fin_milisegundos
					+ "','"
					+ prestadoA.getText()
					+ "', "
					+ valoracion.getRating()
					+ ",'"
					+ formato.getSelectedItem() + "','" + notas.getText() + "')");

			Toast.makeText(this, "Se ha guardado una nueva película",
					Toast.LENGTH_SHORT).show();

		} else {

			ContentValues valores = new ContentValues();
			valores.put("genero", genero.getSelectedItem().toString());
			valores.put("titulo", titulo.getText().toString());
			valores.put("director", director.getText().toString());
			valores.put("idioma", idioma.getSelectedItem().toString());
			valores.put("fecha_ini_prestamo", fecha_inicio_milisegundos);
			valores.put("fecha_fin_prestamo", fecha_fin_milisegundos);
			valores.put("pestado_a", prestadoA.getText().toString());
			valores.put("valoracion", valoracion.getRating());
			valores.put("formato", formato.getSelectedItem().toString());
			valores.put("notas", notas.getText().toString());

			db.update("peliculas", valores, "_id='" + id_pelicula.toString() + "'",
					null);

			Toast.makeText(this,
					"Se ha actualizado la pelicula: " + titulo.getText().toString(),
					Toast.LENGTH_SHORT).show();

		}

		db.close();

	}
}
